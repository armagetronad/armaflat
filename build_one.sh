#!/bin/bash

ID=org.armagetronad.ArmagetronAdvanced
FP=org-armagetronad-ArmagetronAdvanced
SD=`dirname $0`

if test $# -lt 3; then
	echo "Usage: build_one <git branch> <fp_branch> <program name> [optional: force]"
	exit 1
fi

branch=$1
shift
fp_branch=$1
shift
progname=$1
shift

# https://github.com/flatpak/flatpak/pull/2797/files
# This disables parental controls, which cause problems in CI, according to
# https://github.com/flatpak/flatpak/issues/5076#issuecomment-1425841966
export FLATPAK_SYSTEM_HELPER_ON_SESSION=foo

REQUIRE_CHANGES="--require-changes"
# --skip-if-unchanged"
if echo "$@" | grep -q force; then
	REQUIRE_CHANGES=
fi

echo branch=${branch}
echo progname=${progname}
echo fp_branch=${fp_branch}

dir=${progname}-${fp_branch}

set -x
mkdir -p ${dir}
pushd ${dir} || exit $?
if ! test -d ${FP}; then
	git clone https://gitlab.com/armagetronad/org-armagetronad-ArmagetronAdvanced.git || exit $?
	git -C ${FP} submodule init || exit $?
fi

# fetch CI branch
git -C ${FP} fetch || exit $?
git -C ${FP} reset --hard || exit $?
git -C ${FP} checkout ${branch}_ci || exit $?

if ! echo "$@" | grep -q nopull; then
	git -C ${FP} reset origin/${branch}_ci --hard || exit $?

	# merge in human edits for non-stable branches
	if ! echo ${fp_branch} | grep -q stable && ! git -C ${FP} merge --squash origin/${branch}; then
		git -C ${FP} reset --hard || exit $?
	fi

	git -C ${FP} submodule update || exit $?
fi

# edit in proper program name
NEW_ID=org.armagetronad.${progname}
if test ${NEW_ID} != ${ID}; then
	sed < ${FP}/${ID}.json > ${FP}/${NEW_ID}.json -e s/ArmagetronAdvanced/${progname}/g
fi

# build
EXIT=0

REPO=`readlink -f ${HOME}/repo`

# this should do our real builds, whenever the manifest actually changed
SKIP_IF_UNCHANGED=
test -r 'success' && SKIP_IF_UNCHANGED=--skip-if-unchanged

flatpak run --filesystem=host org.flatpak.Builder --install-deps-from=flathub --user --force-clean --default-branch=${fp_branch} --gpg-sign=962C1A2577CB1ACD ${REQUIRE_CHANGES} ${SKIP_IF_UNCHANGED} --repo=${REPO} fp_build ${FP}/${NEW_ID}.json || EXIT=$?
if test ${EXIT} = 42; then
    # just means 'no change'
    EXIT=0
fi

# remember status
if test ${EXIT} = 0; then
    touch 'success'
else
    rm -f 'success'
fi

# cleanup
rm -rf fp_build

exit ${EXIT}
