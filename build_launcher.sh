#!/bin/bash

ID=org.armagetronad.ArmagetronLauncher
SD=`dirname $0`
SD=`readlink -f ${SD}`

REQUIRE_CHANGES="--require-changes"
# --skip-if-unchanged"
if echo "$@" | grep -q force; then
	REQUIRE_CHANGES=
fi

dir=launcher

set -x
mkdir -p ${dir}
pushd ${dir} || exit $?

EXIT=0

FP=${SD}/launcher
NEW_ID=${ID}
fp_branch=stable
REPO=`readlink -f ${HOME}/repo`

# this should do our real builds, whenever the manifest actually changed
SKIP_IF_UNCHANGED=
test -r 'success' && SKIP_IF_UNCHANGED=--skip-if-unchanged

flatpak run --filesystem=host org.flatpak.Builder --install-deps-from=flathub --user --force-clean --default-branch=${fp_branch} --gpg-sign=962C1A2577CB1ACD ${REQUIRE_CHANGES} ${SKIP_IF_UNCHANGED} --repo=${REPO} fp_build ${FP}/${NEW_ID}.json || EXIT=$?
if test ${EXIT} = 42; then
    # just means 'no change'
    EXIT=0
fi

# remember status
if test ${EXIT} = 0; then
    touch 'success'
else
    rm -f 'success'
fi

## cleanup
rm -rf fp_build
popd

exit ${EXIT}
