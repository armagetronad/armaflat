#!/bin/bash

ID=org.armagetronad.ArmagetronAdvanced
FP=org-armagetronad-ArmagetronAdvanced
SD=`dirname $0`

EXIT=0
set -x

flatpak update --user -y

${SD}/build_one.sh stable_0.2.9 stable ArmagetronAdvanced "$@" || EXIT=$?
${SD}/build_one.sh stable_0.2.9 stable_0.2.9 ArmagetronAdvanced "$@" || EXIT=$?

${SD}/build_one.sh rc_0.2.9 rc ArmagetronAdvanced "$@" || EXIT=$?

${SD}/build_one.sh beta_0.2.9 beta ArmagetronBeta "$@" || EXIT=$?
${SD}/build_one.sh beta_0.2.9 beta ArmagetronAdvanced "$@" || EXIT=$?

${SD}/build_one.sh alpha_0.2.9 alpha ArmagetronAlpha "$@" || EXIT=$?
${SD}/build_one.sh alpha_0.2.9 alpha ArmagetronAdvanced "$@" || EXIT=$?

${SD}/build_one.sh experimental_0.4 experimental ArmagetronExperimental "$@" || EXIT=$?

${SD}/build_launcher.sh "$@" || EXIT=$?

flatpak build-update-repo --generate-static-deltas --gpg-sign=962C1A2577CB1ACD --prune-depth 10 --prune repo

exit ${EXIT}
