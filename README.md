# Flatpak installation

    System: see https://flatpak.org/setup/Ubuntu/

    add-apt-repository ppa:flatpak/stable
    apt update
    apt install flatpak flatpak-builder

    flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    flatpak install -y --user flathub runtime/org.freedesktop.Platform/x86_64/22.08 runtime/org.freedesktop.Sdk/x86_64/22.08
    flatpak install -y --user flathub runtime/org.freedesktop.Platform/x86_64/22.08 runtime/org.freedesktop.Sdk/x86_64/23.08
    flatpak install -y --user flathub org.flatpak.Builder

# Runner installation

    gitlab-runner register
    type: shell

The flatpak repository will appear in ~/repo. If you want it somewhere else, make it a symlink before doing anything.
