#!/bin/bash

ID=org.armagetronad.ArmagetronAdvanced
FP=org-armagetronad-ArmagetronAdvanced
SD=`dirname $0`

rm -rf .flatpak-builder */.flatpak-builder

${SD}/build.sh || exit $?

set -x
test -r repo_back || mv repo repo_back
rm -f repo
rm -rf repo

${SD}/build.sh --force || exit $?

rm -rf repo_back/*
mv repo/* repo_back/
rm -rf repo
mv repo_back repo

